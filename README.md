opendap-system
----

## About

OPeNDAP is a standard for accessing geospatial data over a network. It can do slicing and dicing on the data as well as aggregation. Hyrax is the reference implementation of the standard. This project is a collection of scripts and docker configuration for deployment of off-the-shelf software "Hyrax OPeNDAP" in an isolated container, making it easy to maintain and deploy.

The BES configuration is completely stock, but the OLFS was modified to bundle runtime dependencies (java, tomcat, and NGINX), and to point to the BES container. This is useful to individuals or organizations looking to provide OPeNDAP services on their data.

### Using the containers

A docker-compose.yml file is provided for testing. Run `docker-compose up` to run the stock containers.

To create a custom Hyrax stack, create Dockerfiles inheriting from the `nsidc/besd.system` and `nsidc/olfs.system` tags. For your BES container, you'll need to copy in customized configuration files to `/etc/bes`. For OLFS, you may want to customize software configurations at `/etc/nginx` and `/var/lib/tomcat/webapps/opendap`.

## Development Quickstart

### Running project:

1. Clone the repository
2. `cd source`
3. `docker-compose up`

## Development/Release Workflow

This project uses [github flow](https://guides.github.com/introduction/flow/). To begin a feature, start a branch and follow semvar guidelines for versioning.  [bumpversion](https://github.com/peritus/bumpversion) is used to keep the versions in sync across the files in the project.

### Continuous Integration

This project uses [circle CI](https://circleci.com) for CI.

On any commit, Circle will build the container, the test commands `nostests` and `flake8` are currently built-in to the process via the Dockerfile

### Create A Release

TBD
