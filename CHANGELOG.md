# v1.15.4

- Use CMD instead of ENTRYPOINT in docker image
- Update olfs image CA certificates to fix build
- Set up tomcat user to use uid/gid 1000

# v1.15.0

- Improve Tomcat handling of special chars used by OPeNDAP queries, e.g. `[`, `]`

# v1.0.0

- Split system images to their own repository
